require 'json'
require 'socket'
require_relative 'track'
require_relative 'piece'

gameInit = JSON.parse('{"race":{"track":{"id":"germany", "name":"Germany", "pieces":[{"length":100.0}, {"length":100.0, "switch":true}, {"radius":50, "angle":45.0}, {"radius":50, "angle":45.0}, {"radius":50, "angle":45.0}, {"length":50.0}, {"radius":50, "angle":-45.0}, {"radius":50, "angle":-45.0}, {"radius":50, "angle":-45.0}, {"length":50.0}, {"radius":50, "angle":-45.0}, {"radius":50, "angle":-45.0}, {"radius":100, "angle":-22.5}, {"radius":50, "angle":45.0}, {"length":100.0, "switch":true}, {"length":100.0}, {"radius":50, "angle":-45.0}, {"length":50.0}, {"radius":50, "angle":45.0}, {"radius":50, "angle":45.0}, {"radius":100, "angle":22.5}, {"length":100.0, "switch":true}, {"length":100.0}, {"radius":50, "angle":45.0}, {"radius":50, "angle":45.0}, {"radius":50, "angle":45.0}, {"radius":50, "angle":45.0}, {"length":100.0, "switch":true}, {"radius":50, "angle":-45.0}, {"radius":50, "angle":-45.0}, {"length":50.0}, {"radius":100, "angle":22.5}, {"length":100.0}, {"length":50.0}, {"radius":100, "angle":-22.5}, {"radius":100, "angle":-22.5}, {"length":100.0, "switch":true}, {"radius":50, "angle":45.0}, {"radius":50, "angle":45.0}, {"radius":100, "angle":22.5}, {"length":100.0}, {"length":100.0}, {"length":100.0}, {"radius":100, "angle":45.0}, {"length":70.0}, {"length":100.0, "switch":true}, {"radius":50, "angle":-45.0}, {"radius":50, "angle":-45.0}, {"radius":100, "angle":45.0}, {"radius":100, "angle":45.0}, {"length":50.0}, {"radius":100, "angle":45.0}, {"radius":100, "angle":45.0}, {"radius":100, "angle":45.0, "switch":true}, {"length":100.0}, {"length":100.0}, {"length":59.0}], "lanes":[{"distanceFromCenter":-10, "index":0}, {"distanceFromCenter":10, "index":1}], "startingPoint":{"position":{"x":-12.0, "y":192.0}, "angle":-90.0}}, "cars":[{"id":{"name":"Joni", "color":"red"}, "dimensions":{"length":40.0, "width":20.0, "guideFlagPosition":10.0}}], "raceSession":{"laps":3, "maxLapTimeMs":60000, "quickRace":true}}}')

carPositions = JSON.parse('{"id":{"name":"Joni", "color":"red"}, "angle":0.0, "piecePosition":{"pieceIndex":1, "inPieceDistance":0.0, "lane":{"startLaneIndex":0, "endLaneIndex":0}, "lap":0}}')


a = Track.new gameInit
a.choose_lane(carPositions)

pieces = JSON.parse('[{"length": 100.0},{"length": 100.0,"switch": true},{"radius": 200,"angle": 22.5}]')
p = Piece.new pieces[2]
