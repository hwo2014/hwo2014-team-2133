class Track

  attr_accessor :pieces, :lanes, :last_change, :current_piece, :throttle_value

  def initialize(json)
    nimi = json["race"]["track"]["name"]
    pituus = json["race"]["track"]["pieces"].count
    autoja = json["race"]["cars"].count
    @lanes = json["race"]["track"]["lanes"].count
    @pieces = json["race"]["track"]["pieces"]

    @throttle_value = 0.7

    puts "rata #{nimi}, pituus #{pituus} palikkaa"
    puts "kaistoja #{@lanes} autoja #{autoja}"
  end

  def length
    d = 0.0
    @pieces.each do |i|
      p = Piece.new i
      d += p.distance
    end
    d
  end

  def choose_lane(json)
    index          = json["piecePosition"]["pieceIndex"]
    distance       = json["piecePosition"]["inPieceDistance"]
    lane_start     = json["piecePosition"]["lane"]["startLaneIndex"]
    lane_end       = json["piecePosition"]["lane"]["endLaneIndex"]
    rightmost_lane = @lanes - 1

    # Ei tehdä mitään jos nyt ei pidä tehdä switchä
    return false unless do_switch?(index)

    # Otetaan seuraava ja sitäseuraava switch
    s1 = next_switch(index)
    s2 = next_switch(s1)

    # Katotaan kumalla kaistalla kantsii olla switchien välissä
    turn_direction = left_or_right(s1, s2)

    # Otetaan talteen blokki jossa ollaan tehty swicth, ettei tehdä uudestaan samassa
    @last_change = index

    if turn_direction == "right" && lane_end != rightmost_lane
      return "Right"
    elsif turn_direction == "left" && lane_end != 0
      return "Left"
    end

    false
  end

  def left_or_right(from_piece, to_piece)
    piece = from_piece

    # Kerätään dataa näihin
    left_angle = 0.0
    right_angle = 0.0

    # Loopataan anettujen palojen väli ja summatana angle
    until piece == next_index(to_piece)
      p = Piece.new @pieces[piece]

      left_angle  += p.angle if p.left?
      right_angle += p.angle if p.right?

      piece = next_index(piece)
    end

    # Kumpaan suuntaan on enemmän käännöstä, kantsii olla sillä radalla
    left_angle > right_angle ? "left" : "right"
  end

  def do_switch?(piece)
    # Switch pitää tehdä, jos seuraava piece on switch ja ei olla tässä piecessä switchattu
    (next_piece(piece)["switch"] && @last_change != piece) ? true : false
  end

  def next_switch(piece)
    s = nil

    while s.nil?
      piece = next_index(piece)
      s = @pieces[piece]["switch"]
    end

    piece
  end

  def kuinka_tiukkaa(json)
    piece = json["piecePosition"]["pieceIndex"]

    i1 = next_index(piece)
    i2 = next_index(i1)

    # Otetaan nykyinen sekä kaks pieceä eteenpäin
    p1 = Piece.new @pieces[piece]
    p2 = Piece.new @pieces[i1]
    p3 = Piece.new @pieces[i2]

    # Paljon ollaan matkustettu tästä palikasta
    pd = json["piecePosition"]["inPieceDistance"].to_f
    td = p1.distance
    percentage = (td - pd) / 100

    # Lasketaan paljo on tulossa mutkaa
    tuikka = ((p1.angle * percentage) + p2.angle).abs

    puts "tiukkuus #{tuikka}"

    if tuikka.zero?
      1
    elsif tuikka > 100
      0.2
    else
      (1 - (tuikka / 100)).round(3)
    end

  end

  def next_index(piece)
    piece += 1
    @pieces[piece] ? piece : 0
  end

  def next_piece(piece)
    index = next_index(piece)
    @pieces[index]
  end

  def alter_throttle(json)
    angle = json["angle"].to_f.abs
    index = json["piecePosition"]["pieceIndex"]

    if angle >= 40.0
      @throttle_value = @throttle_value >= 0.6 ? 0.5 : 0.5
    elsif angle >= 20.0
      @throttle_value = 0.825
    else
      @throttle_value = 0.825
    end

    current_piece   = @pieces[index]
    next_piece      = next_piece(index)
    next_next_piece = @pieces[next_index(next_index(index))]

    if current_piece && !current_piece["angle"]
      if next_piece && !next_piece["angle"]
        if next_next_piece && !next_next_piece["angle"]
          #puts "piece: #{current_piece} next_piece: #{next_piece} next_next_piece: #{next_next_piece}"
          @throttle_value = 0.87
        end
      end
    end

    @throttle_value

    #palautetaan aina 0.6 :)
    0.65
  end

end
