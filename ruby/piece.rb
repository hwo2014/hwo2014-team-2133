class Piece

  attr_accessor :length, :switch, :radius, :angle_raw

  def initialize(json)
    self.length = json["length"].to_f || 0.0
    self.switch = json["switch"] || false
    self.radius = json["radius"].to_f || 0.0
    self.angle_raw = json["angle"].to_f || 0.0
  end

  def angle
    angle_raw.abs
  end

  def left?
    angle_raw < 0
  end

  def right?
    angle_raw > 0
  end

  def straight?
    angle.zero?
  end

  def distance
    # distance = angle / 360 * 2 * pi * radius
    length.zero? ? (angle / 360 * 2 * Math::PI * radius) : length
  end

end
