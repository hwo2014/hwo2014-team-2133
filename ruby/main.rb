require 'json'
require 'socket'
require_relative 'track'
require_relative 'piece'

server_host = ARGV[0] || "hakkinen.helloworldopen.com"
server_port = ARGV[1] || "8091"
bot_name = ARGV[2] || %x( whoami ).chomp
bot_key = ARGV[3] || "+BlftXT5J1TZdQ"

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"

class Herukas

  attr_accessor :my_car_color, :msgTick, :pieceIndex, :piecescount, :lap

  def initialize(server_host, server_port, bot_name, bot_key)
    @pieceIndex = 0
    @piecescount = 0
    @lap = 0
    tcp = TCPSocket.open(server_host, server_port)
    play(bot_name, bot_key, tcp)
  end

  private

    def play(bot_name, bot_key, tcp)
      tcp.puts join_race(bot_name, bot_key)
      react_to_messages_from_server tcp
    end

    def react_to_messages_from_server(tcp)
      while json = tcp.gets

        message = JSON.parse(json)
        msgType = message['msgType']
        msgData = message['data']
        @msgTick = message['gameTick'].to_i

        case msgType
          when 'carPositions'
            # Haetaan meiän auto on messagesta
            our_car = fetch_our_car msgData

            # Muutama instassimuuttuja echottelua varten
            @pieceIndex = our_car["piecePosition"]["pieceIndex"]
            @lap = our_car["piecePosition"]["lap"]

            # Tehdään AI!
            throttle_value = @track.kuinka_tiukkaa(our_car)
            direction = @track.choose_lane(our_car)

            # Jos pitää vaihtaa kaistaa ni vaihdetaan, muuten kaasutellaan
            if direction
              tcp.puts change_lane(direction)
            else
              tcp.puts throttle_message(throttle_value)
            end

          else
            case msgType
              when 'join'
                puts 'Joined'
              when 'yourCar'
                @my_car_color = msgData["color"]
                puts "Me ollaan #{my_car_color}"
              when 'gameInit'
                @track = Track.new msgData
                @piecescount = msgData["race"]["track"]["pieces"].count
              when 'gameStart'
                puts 'Race started'
              when 'crash'
                puts 'Someone crashed'
              when 'gameEnd'
                puts 'Race ended'
              when 'error'
                puts "ERROR: #{msgData}"
              else
                puts "Got: #{msgType}"
                puts "Data: #{msgData}"
            end

            # ping!
            tcp.puts ping_message
        end
      end
    end

    def join_message(bot_name, bot_key)
      make_msg("join", {:name => bot_name, :key => bot_key})
    end

    def join_race(bot_name, bot_key)
      make_msg("joinRace", { :botId => { :name => bot_name, :key => bot_key },
                             :trackName => "keimola",
                             :password => "kala",
                             :carCount => 3 })
    end

    def create_race(bot_name, bot_key)
      make_msg("createRace", { :botId => { :name => bot_name, :key => bot_key },
                               :trackName => "usa",
                               :password => "kala",
                               :carCount => 1 })
    end

    def change_lane(direction)
      make_msg("switchLane", direction)
    end

    def throttle_message(throttle)
      make_msg("throttle", throttle)
    end

    def ping_message
      make_msg("ping", {})
    end

    def make_msg(msgType, data)
      message = {:msgType => msgType, :data => data, :gameTick => @msgTick}
      puts "lap: #{@lap+1} piece: #{@pieceIndex+1}/#{@piecescount} message: #{message}"
      JSON.generate(message)
    end

    def fetch_our_car(json)
      json.select { |car| car["id"]["color"] == @my_car_color }.first
    end

end

Herukas.new(server_host, server_port, bot_name, bot_key)
